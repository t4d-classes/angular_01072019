import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FirstService } from './services/first.service';
import { SapphireComponent } from './components/sapphire/sapphire.component';
import { EmeraldComponent } from './components/emerald/emerald.component';
import { NameFormComponent } from './components/name-form/name-form.component';

@NgModule({
  declarations: [
    AppComponent,
    SapphireComponent,
    EmeraldComponent,
    NameFormComponent
  ],
  imports: [
    BrowserModule, ReactiveFormsModule,
  ],
  providers: [
    FirstService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
