import { Component, OnInit } from '@angular/core';
import { FirstService } from 'src/app/services/first.service';

@Component({
  selector: 'app-emerald',
  templateUrl: './emerald.component.html',
  styleUrls: ['./emerald.component.css']
})
export class EmeraldComponent implements OnInit {

  constructor(private firstSvc: FirstService) {
    this.firstSvc.doIt();
  }

  ngOnInit() {
  }

}
