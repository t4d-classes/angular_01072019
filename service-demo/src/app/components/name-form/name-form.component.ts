import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, FormControl } from '@angular/forms';

@Component({
  selector: 'app-name-form',
  templateUrl: './name-form.component.html',
  styleUrls: ['./name-form.component.css'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => NameFormComponent), multi: true, }
  ]
})
export class NameFormComponent implements ControlValueAccessor {

  firstNameInput = new FormControl();
  lastNameInput = new FormControl();

  _onChange: (value: string) => void;
  _onTouched: () => void;

  constructor() { }

  writeValue(obj: any): void {
    const nameParts = String(obj).split(' ');
    this.firstNameInput.setValue(nameParts[0]);
    this.lastNameInput.setValue(nameParts[1]);
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    if (isDisabled) {
      this.firstNameInput.disable();
      this.lastNameInput.disable();
    } else {
      this.firstNameInput.enable();
      this.lastNameInput.enable();
    }
  }

  doChange() {
    if (!this.firstNameInput.value) {
      this._onChange(this.lastNameInput.value);
    } else {
      this._onChange(this.firstNameInput.value + ' ' + this.lastNameInput.value);
    }
  }

  doTouched() {
    this._onTouched();
  }

}
