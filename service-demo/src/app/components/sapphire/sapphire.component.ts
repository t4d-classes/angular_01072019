import { Component, OnInit, InjectionToken, Inject } from '@angular/core';
import { FirstService, ILogSvc } from 'src/app/services/first.service';

const LogSvcToken = new InjectionToken('log svc');

@Component({
  selector: 'app-sapphire',
  templateUrl: './sapphire.component.html',
  styleUrls: ['./sapphire.component.css'],
  providers: [ { provide: LogSvcToken, useValue: { doIt: () => console.log('did it too') } } ],
})
export class SapphireComponent implements OnInit {

  constructor(@Inject(LogSvcToken) private logSvc: ILogSvc) {
    this.logSvc.doIt();
  }

  ngOnInit() {
  }

}
