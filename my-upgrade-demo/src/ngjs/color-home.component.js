import template from './color-home.component.html';

export const ColorHomeComponent = {
  template,
  controller: class {

    constructor(colorsSvc2) {
      this.headerText = 'Color Tool';
      console.log('hi');
      this.colors = colorsSvc2.all();
      console.log('hi');
      this.newColor = '';
    }

    addColor() {
      this.colors = this.colors.concat(this.newColor);
      this.newColor = '';
    }

  }
};