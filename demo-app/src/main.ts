import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import { Observable, Observer, fromEvent } from 'rxjs';
import { map, filter, take } from 'rxjs/operators';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));


// // const s = fromEvent(document.querySelector('#some-button'), 'click');

// // s.subscribe(() => {

// // });

// let outerCounter = 0;


// // callback runs on subscribe
// // each subscriber gets it owns callback invocation
// // the values are not shared
// const o: Observable<number> = Observable.create( (observer: Observer<number>) => {

//   const myOuterCounter = outerCounter++;

//   console.log('observable running: ', myOuterCounter);

//   // document.querySelector('#some-button').addEventListener('click', (e) => {
//   //   observer.next(e);
//   // });

//   let counter = 0;
//   const h = setInterval(() => {
//     console.log('outer: ', myOuterCounter, 'inner: ', counter);

//     // if (counter > 5) {
//     //   observer.error('number too high');
//     //   clearInterval(h);
//     //   return;
//     // }

//     console.log('closed: ', observer.closed);

//     if (observer.closed) {
//       clearInterval(h);
//     }

//     observer.next(counter++);
//   }, 1000);

//   setTimeout(() => {
//     clearInterval(h);
//     observer.complete();
//   }, 20000);
// });

// const subscriber = o.pipe(
//   map(n => n * 2),
//   // filter(n => n > 5),
//   take(5),
// ).subscribe(num => {
//   console.log('callback: ', num);
// }, err => {
//   console.log('error: ', err);
// }, () => {
//   console.log('completed');
// });

// // setTimeout(() => {
// //   subscriber.unsubscribe();
// // }, 6000);


// // o.subscribe(num => {
// //   // console.log(num);
// // });


// // callback runs immediately
// // callback runs once
// // all .then's share the same result
// // const p = new Promise(resolve => {

// //   console.log('promise running');

// //   setTimeout(() => {
// //     console.log('promise value: 0');
// //     resolve(0);
// //   }, 2000);

// // });

// // p.then(() => {

// // });

// // p.then(() => {

// // });


