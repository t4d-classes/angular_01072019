import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Step 1 - Import the module for JS purposes
import { ColorToolModule } from './color-tool/color-tool.module';
import { CarToolModule } from './car-tool/car-tool.module';
import { FormDemoModule } from './form-demo/form-demo.module';
import { SharedModule } from './shared/shared.module';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    // Step 2 - Import the module for Angular purposes
    BrowserModule, ColorToolModule, CarToolModule, FormDemoModule, SharedModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
