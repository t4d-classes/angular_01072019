import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myUppercase',
  pure: true,
})
export class MyUppercasePipe implements PipeTransform {

  transform(value: any) {
    return String(value).toUpperCase();
  }

}
