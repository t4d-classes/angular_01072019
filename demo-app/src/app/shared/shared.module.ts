import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ToolHeaderComponent } from './components/tool-header/tool-header.component';
import { MyUppercasePipe } from './pipes/my-uppercase.pipe';
import { MyAppendPipe } from './pipes/my-append.pipe';
import { TypeaheadDemoComponent } from './components/typeahead-demo/typeahead-demo.component';


@NgModule({
  declarations: [ ToolHeaderComponent, MyUppercasePipe, MyAppendPipe, TypeaheadDemoComponent ],
  imports: [
    CommonModule, ReactiveFormsModule, HttpClientModule,
  ],
  exports: [ ToolHeaderComponent, MyUppercasePipe, TypeaheadDemoComponent ],
})
export class SharedModule { }
