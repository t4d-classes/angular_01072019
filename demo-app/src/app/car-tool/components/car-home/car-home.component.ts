import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { concatMapTo, map, take} from 'rxjs/operators';

import { Car } from '../../models/car';
import { CarsService } from '../../services/cars.service';

@Component({
  selector: 'car-home',
  templateUrl: './car-home.component.html',
  styleUrls: ['./car-home.component.css']
})
export class CarHomeComponent implements OnInit {

  errorMessage = '';
  headerText = 'Car Tool';

  cars: Car[] = [];

  editCarId = 0;

  // Using the subscribe function, no piping needed, change the promise-based code to use the RxJS subscribe API
  // Hint: subscribe does not return an observable

  constructor(private carsSvc: CarsService) { }

  refreshCars(mutate?: Observable<any>) {

    const pipes = [
      concatMapTo(this.carsSvc.all()),
      map( (cars: Car[]) => cars.slice(0, 3)),
    ];

    // RxJS 4 and earlier, if you this code in stack overflow or older blogs ignore it
    // obs.map().filter().take().subscribe()

    (mutate || of(null))
      .pipe(...pipes as [])
      .subscribe(cars => { this.cars = cars; }, err => {
        console.log(err);
        this.cars = [];
        this.errorMessage = 'Error: Loading Cars...';
      }, () => {
        this.errorMessage = '';
        this.editCarId = 0;
      });
  }

  ngOnInit() {
    this.refreshCars();
  }
  doRefreshCars() {
    this.refreshCars();
  }
  doEditCar(carId) {
    this.editCarId = carId;
  }
  doCancelCar() {
    this.editCarId = 0;
  }
  doDeleteCar(carId) {
    this.refreshCars(this.carsSvc.delete(carId));
  }
  doReplaceCar(car: Car) {
    this.refreshCars(this.carsSvc.replace(car));
  }

  doAddCar(newCar: Car) {

    this.refreshCars(this.carsSvc.append(newCar));

    // this.carsSvc
    //   .append(newCar)
    //   .pipe(concatMapTo(this.carsSvc.all()))
    //   .subscribe(cars => {
    //     this.cars = cars;
    //   }, err => {
    //     console.log(err);
    //     this.cars = [];
    //     this.errorMessage = 'Error: Loading Cars...';
    //   }, () => {
    //     this.errorMessage = '';
    //     this.editCarId = 0;
    //   });

    // this.carsSvc.append(newCar).subscribe(() => {
    //   this.carsSvc.all().subscribe(cars => {
    //     this.cars = cars;
    //   });
    // });
  }

}
