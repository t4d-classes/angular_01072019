import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ReactiveFormsModule,
  FormsModule
} from '@angular/forms';


import { SampleFormComponent } from './components/sample-form/sample-form.component';
import { SampleTplFormComponent,  MyRequiredValidatorDirective } from './components/sample-tpl-form/sample-tpl-form.component';

@NgModule({
  declarations: [SampleFormComponent, SampleTplFormComponent, MyRequiredValidatorDirective],
  imports: [
    CommonModule, ReactiveFormsModule,
    FormsModule,
  ],
  exports: [SampleFormComponent, SampleTplFormComponent],
})
export class FormDemoModule { }
