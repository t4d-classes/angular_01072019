import * as angular from 'angular';
import { downgradeComponent, downgradeInjectable } from '@angular/upgrade/static';

import { ColorsService } from './colors.service';
import { ColorHomeComponent } from './color-home.component';
import { ToolHeaderComponent } from './tool-header.component';

// import { ColorsService } from '../app/services/colors.service';
import { ToolHeaderComponent as ToolHeaderComponentUpgrade } from '../app/components/tool-header/tool-header.component';
import { ColorListComponent } from '../app/components/color-list/color-list.component';

angular.module('colorToolApp', [])
  .component('colorHome', ColorHomeComponent)
  .component('toolHeader', ToolHeaderComponent)
  .directive('toolHeaderUpgrade', downgradeComponent({ component: ToolHeaderComponentUpgrade }))
  .directive('colorList', downgradeComponent({  component: ColorListComponent }))
  .service('colorsSvc', ColorsService)
  // .factory('colorsSvc', downgradeInjectable(ColorsService));