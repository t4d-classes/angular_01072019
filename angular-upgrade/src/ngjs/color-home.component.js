import template from './color-home.component.html';

export const ColorHomeComponent = {
  template,
  controller: class {
    
    constructor(colorsSvc) {
      this.headerText = 'Color Tool';
      this.colors = colorsSvc.all();
      this.newColor = '';
    }

    addColor() {
      this.colors = this.colors.concat(this.newColor);
      this.newColor = '';
    }
  }
};
