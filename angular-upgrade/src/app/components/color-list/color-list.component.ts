import { Component, OnInit, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
  useFactory: (i: { get: (svcName: string) => object }) => i.get('colorsSvc'),
  deps: ['$injector'],
})
abstract class ColorsService {
  abstract all(): string[];
}

@Component({
  selector: 'color-list',
  templateUrl: './color-list.component.html',
  styleUrls: ['./color-list.component.css'],
  // providers: [
  //   {
  //     provide: ColorsService,
  //     useFactory: (i: { get: (svcName: string) => object }) => i.get('colorsSvc'),
  //     deps: ['$injector']
  //   },
  // ]
})
export class ColorListComponent implements OnInit {

  colors: string[] = [];

  constructor(private colorsSvc: ColorsService) { }

  ngOnInit() {
    this.colors = this.colorsSvc.all();
  }

}
