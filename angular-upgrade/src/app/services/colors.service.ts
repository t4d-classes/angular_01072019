import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ColorsService {

  _colors: string[];

  constructor() {
    this._colors = [ 'red', 'green', 'blue' ];
  }

  all() {
    console.log('angular colors svc');
    return this._colors.concat();
  }
}
