import { Directive, ElementRef, Injector, Input } from '@angular/core';
import { UpgradeComponent } from '@angular/upgrade/static';

@Directive({
  selector: 'tool-header-legacy'
})
export class ToolHeaderLegacyDirective extends UpgradeComponent {

  @Input() headerText: string;

  constructor(elementRef: ElementRef, injector: Injector) {
    super('toolHeader', elementRef, injector);
  }
}
